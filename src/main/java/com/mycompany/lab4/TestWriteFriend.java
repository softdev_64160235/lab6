/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 *
 * @author Love_
 */
public class TestWriteFriend {
    public static void main(String[] args) throws FileNotFoundException, IOException {
        Friend f1 = new Friend("Khongdet",20,"0921546587");
        Friend f2 = new Friend("Aef",20,"0985487456");
        System.out.println(f1);
        System.out.println(f2);
        File file = new File("friends.dat");
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(f1);
        oos.writeObject(f2);
        oos.close();
        fos.close();
    }
}
